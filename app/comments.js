const express = require('express');
const path = require('path');
const multer = require('multer');
const fileDb = require('../fileDb');
const {nanoid} = require('nanoid');
const router = express.Router();
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
   try {
       if (req.query.datetime) {
           const datetime = new Date(req.query.datetime);
           if (isNaN(datetime.getDate())) {
               return res.status(400).send({message: 'Error with Date'});
           }
           return res.send(fileDb.getItemByDate(req.query.datetime));
       }
       const comments = await fileDb.getItems();
       res.send(comments);
   } catch (e) {
       console.log(e);
   }
});

router.post('/', upload.single('image'), async (req, res) => {
   try {
       const comment = req.body;

       if (req.file) {
           comment.image = req.file.filename;
       }

       await fileDb.addItem(comment);

       res.send(comment);
   } catch (e) {
       console.log(e);
   }
});

module.exports = router;