const fs = require('fs').promises;
const {nanoid} = require('nanoid');

const filename = './db.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContents = await fs.readFile(filename);
            data = JSON.parse(fileContents);
        } catch (e) {
            data = [];
        }
    },
    async getItems() {
        return data;
    },
    async addItem(item) {
        item.id = nanoid();
        item.datetime = new Date().toISOString();
        data.push(item);
        await this.save();
    },
    async  getItemByDate(datetime) {
        const dateArr = [];
        for (let item of data) {
            if (item.datetime > datetime) {
                dateArr.push(item);
            }
        }
        return dateArr;
    },
    async save() {
        await fs.writeFile(filename, JSON.stringify(data, null, 2));
    }
};